const express = require('express');
const app = express();
const port = process.env.HTTP_PORT || 3000;

app.get('/', (req, res) => res.send('It works!'));

app.get('/healthy', (req, res) => res.send('healthy'));

app.get('/started', (req, res) => res.send('started'));

app.listen(port, () => console.log(`Listening on port ${port}`));
