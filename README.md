# deploy-server

# Installation Steps

1. Deploy a DO droplet.
2. Install docker, nvm, node.
3. Generate an RSA key.
```bash
$ ssh-keygen -t rsa
```
4. Configure SSH client to find your GitLab private SSH in the server.
```bash
$ eval $(ssh-agent -s)
```
5. Add your key to the private SSH registry.
```bash
$ ssh-add ~/.ssh/id_rsa
```
6. Set SSH settings.
```bash
$ vim ~/.ssh/config

# GitLab.com server
Host gitlab.com
RSAAuthentication yes
IdentityFile ~/.ssh/id_rsa
```
7. Add the Server Key as a deployment key in your Repository configuration.
```bash
$ cat ~/.ssh/id_rsa.pub
```
Add this under Settings, Repository, Deploy Keys
8. Provide the SSH private key to Gitlab for deployment.
```bash
$ cat ~/.ssh/id_rsa
```
Add this under Settings, CI/CD, Variables as SSH_PRIVATE_KEY.
Also add the DO droplet's IP address as DEPLOY_SERVER.

9. Checkout the code in the target directory.
```bash
$ cd /src/
$ git clone git@gitlab.com:username/repositoryname.git
```

10. Run the deploy pipeline for the droplet to ensure it works.

# Additional Steps

**(Unverified)**

* In `~/.ssh/config`, use `PubkeyAuthentication yes` instead of `RSAAuthentication`
* In `/etc/ssh/sshd_config`, enable `PubkeyAuthentication yes`; restart sshd via `service sshd restart`

# References

https://ourcodeworld.com/articles/read/654/how-to-create-and-configure-the-deployment-ssh-keys-for-a-gitlab-private-repository-in-your-ubuntu-server
https://about.gitlab.com/2016/04/19/how-to-set-up-gitlab-runner-on-digitalocean/

